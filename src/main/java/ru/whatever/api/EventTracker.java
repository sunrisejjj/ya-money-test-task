package ru.whatever.api;

import java.time.LocalDateTime;

/**
 * Event tracker component interface.
 * It's up to component's implementation to decide
 * how exactly events must be considered as actual or non-actual.
 */
public interface EventTracker {

    /**
     * Tracks single event is it is considered as actual or non-actual.
     *
     * @param eventTime time when event happened.
     */
    void track(LocalDateTime eventTime);

    /**
     * @return count of events that occurred during last minute.
     */
    long withinLastMinute();

    /**
     * @return count of events that occurred during last hour.
     */
    long withinLastHour();

    /**
     * @return count of events that occurred during last day.
     */
    long withinLastDay();

}
