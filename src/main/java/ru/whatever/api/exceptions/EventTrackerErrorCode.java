package ru.whatever.api.exceptions;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Enumeration represents various error codes that allows to distinguish
 * various reasons of exceptions happened during tracking of event.
 */
@RequiredArgsConstructor
public enum EventTrackerErrorCode {

    /**
     * Error code used to represents that event can't be stored
     * because event tracker's internal storage is already full.
     */
    TOO_MANY_EVENTS("ERR01"),

    /**
     * Error code used in case when it is not possible to distinguish
     * exact reason of occurred exception.
     */
    UNKNOWN("ERR99");

    @Getter private final String code;

}
