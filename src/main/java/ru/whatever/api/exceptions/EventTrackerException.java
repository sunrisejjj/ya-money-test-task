package ru.whatever.api.exceptions;

import lombok.Getter;

import static ru.whatever.api.exceptions.EventTrackerErrorCode.*;

/**
 * Base exception class for all exceptions related to event tracking.
 * Supplies error code which is an instance of {@link EventTrackerErrorCode}
 * to allow clients more precisely distinguish root cause of errors.
 */
public class EventTrackerException extends RuntimeException {

    @Getter private EventTrackerErrorCode errorCode;

    public EventTrackerException(String message, EventTrackerErrorCode errorCode) {
        super(message);
        this.errorCode = errorCode != null ? errorCode : UNKNOWN;
    }

}
