package ru.whatever.api.exceptions;

import static ru.whatever.api.exceptions.EventTrackerErrorCode.*;

/**
 * Exception represents that event can't be tracked by event tracker
 */
public class StorageLimitException extends EventTrackerException {

    private static final String MSG_TEMPLATE
            = "Storage is full, already contains %d elements";

    public StorageLimitException(int storageCapacity) {
        super(String.format(MSG_TEMPLATE, storageCapacity), TOO_MANY_EVENTS);
    }

}
