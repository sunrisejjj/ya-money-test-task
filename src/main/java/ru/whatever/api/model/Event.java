package ru.whatever.api.model;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * Immutable data structure that represents tracked event. May be extended
 * to hold additional data like event ID, name, source, etc.
 * In case of extending class with other fields Builder pattern
 * should be considered to make object creation less painful.
 */
@Data
public final class Event {

    /** Date and time when event happened */
    private final LocalDateTime eventDateTime;

    public boolean isBefore(LocalDateTime dateTime) {
        return eventDateTime.isBefore(dateTime);
    }

    public boolean isAfter(LocalDateTime dateTime) {
        return eventDateTime.isAfter(dateTime);
    }

}
