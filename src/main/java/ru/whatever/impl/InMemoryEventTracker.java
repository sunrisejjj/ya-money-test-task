package ru.whatever.impl;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import ru.whatever.api.exceptions.StorageLimitException;
import ru.whatever.api.model.Event;
import ru.whatever.api.EventTracker;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

/**
 * Implementation of event tracker component interface.
 * Internally uses {@link ArrayList} to store events.
 * As a result no more than <code>Integer.MAX_VALUE</code> events may be stored.
 * <br/>
 * Data structure that is used internally is not synchronized
 * so all operations are guarded with locks.
 * This implementation of event tracker uses {@link ReentrantReadWriteLock}
 * to guard stored events.
 */
@Slf4j
public final class InMemoryEventTracker implements EventTracker {

    /** Event time-to-live parameter. Default value is 86400 seconds. */
    @Getter @Setter private int eventTimeToLive = 60 * 60 * 24;

    /**
     * Count of events after which check if clean-up is necessary
     * will be performed. Default value if 10000.
     */
    @Getter @Setter private int cleanupCheckThreshold = 10000;

    /**
     * Count of stale events after which storage cleanup will be performed.
     * Default value if 1000.
     */
    @Getter @Setter private int initCleanupThreshold = 1000;

    /** Read-write lock used to guard access to events storage */
    private final ReentrantReadWriteLock readWriteLock
            = new ReentrantReadWriteLock();

    /** Shortcut reference to write lock */
    private final Lock writeLock = readWriteLock.writeLock();

    /** Shortcut reference to read lock */
    private final Lock readLock = readWriteLock.readLock();

    /** Reference to events storage */
    private List<Event> events;

    public InMemoryEventTracker() {
        this.events = new ArrayList<>();
    }

    InMemoryEventTracker(List<Event> storage) {
        this.events = storage;
    }

    /**
     * Tracks single event within in-memory storage.
     * Additionally checks if storage cleanup is required based on
     * event time-to-leave and check threshold.
     * Additionally performs storage cleanup based on
     * init cleanup threshold if cleanup is necessary.
     *
     * @param eventTime time when event happened.
     * @throws IllegalArgumentException if event date-time is not specified
     * @throws StorageLimitException    if storage cannot store any more events
     *                                  even after cleanup
     */
    @Override
    public void track(LocalDateTime eventTime) {
        if (eventTime == null) {
            throw new IllegalArgumentException("Event time is not specified");
        }
        if (eventTime.isBefore(LocalDateTime.now()
                .minusSeconds(eventTimeToLive))) {
            if (log.isDebugEnabled()) {
                log.debug("Stale event received: " + eventTime);
            }
            return;
        }

        storeEvent(new Event(eventTime));
    }

    private void storeEvent(Event eventToStore) {
        try {
            log.trace("Acquiring write lock");
            writeLock.lock();

            cleanupIfNeeded();
            if (events.size() == Integer.MAX_VALUE) {
                throw new StorageLimitException(events.size());
            }

            if (log.isTraceEnabled()) {
                log.trace("Storing event " + eventToStore);
            }
            events.add(eventToStore);
            if (log.isTraceEnabled()) {
                log.trace("Event stored: " + eventToStore);
            }
        } finally {
            log.trace("Releasing write lock");
            writeLock.unlock();
        }
    }

    /**
     * Storage cleanup is performed in a following way:
     * first check is cheap - storage size will be checked against
     * <code>cleanupCheckThreshold</code> parameter value.
     * If first check passed successfully then more expensive second check
     * will be performed - is stale events count greater than
     * <code>initCleanupThreshold</code> parameter.
     * If second check is passed then storage cleanup will be performed.
     * <br/>
     * For the sake of simplicity it was decided to implement cleanup check
     * as a part of event tracking procedure - thread already holds write lock
     * so less lock contention expected comparing to case
     * when storage size and count of stale events will be checked separately,
     * producing more read locks.
     */
    private void cleanupIfNeeded() {
        if (events.size() >= cleanupCheckThreshold) {
            LocalDateTime latestAllowedDateTime = LocalDateTime.now()
                    .minusSeconds(eventTimeToLive);
            if (log.isTraceEnabled()) {
                log.trace("latestAllowedDateTime = " + latestAllowedDateTime
                        + "storage size = " + events.size()
                        + "cleanupCheckThreshold = " + cleanupCheckThreshold
                        + "initCleanupThreshold = " + initCleanupThreshold);
            }

            log.debug("Check if storage size is above cleanupCheckThreshold");
            long staleEventsCount = events.stream()
                    .filter(event -> event.isBefore(latestAllowedDateTime))
                    .count();

            if (staleEventsCount >= initCleanupThreshold) {
                log.debug("Stale events exist, preserving only actual events");
                events = events.stream()
                        .filter(event -> !event.isBefore(latestAllowedDateTime))
                        .collect(Collectors.toCollection(ArrayList::new));
            } else {
                log.debug("Stale events are not exist");
            }
        } else {
            log.debug("Event storage size is below cleanupCheckThreshold");
        }
    }

    /** {@inheritDoc} */
    @Override
    public long withinLastMinute() {
        return countEventsLaterThan(LocalDateTime.now().minusMinutes(1));
    }

    /** {@inheritDoc} */
    @Override
    public long withinLastHour() {
        return countEventsLaterThan(LocalDateTime.now().minusHours(1));
    }

    /** {@inheritDoc} */
    @Override
    public long withinLastDay() {
        return countEventsLaterThan(LocalDateTime.now().minusDays(1));
    }

    private long countEventsLaterThan(LocalDateTime time) {
        try {
            readLock.lock();
            return events.stream()
                    .filter(event -> !event.isBefore(time))
                    .count();
        } finally {
            readLock.unlock();
        }
    }

}
