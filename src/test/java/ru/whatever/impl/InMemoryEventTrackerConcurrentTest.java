package ru.whatever.impl;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.concurrent.CountDownLatch;

import static org.junit.jupiter.api.Assertions.*;

@Tag("ConcurrencyCheck")
public class InMemoryEventTrackerConcurrentTest {

    private static final int THREADS_COUNT = 100;
    private static final int EVENTS_TO_EMIT = 500;

    private InMemoryEventTracker systemUnderTest = new InMemoryEventTracker();

    @Test
    void testTrackEventsConcurrently() throws Exception {
        // start latch to simulate higher-concurrency situation
        final CountDownLatch initDone = new CountDownLatch(THREADS_COUNT);
        // stop latch to do asserts only when concurrent execution done
        final CountDownLatch emissionDone = new CountDownLatch(THREADS_COUNT);

        for (int idx = 0; idx < THREADS_COUNT; idx++) {
            new Thread(createRunnableForTest(initDone, emissionDone)).start();
            initDone.countDown();
        }

        for (int checkIdx = 0; checkIdx < 100; checkIdx++) {
            // acquire some read-locks in between
            systemUnderTest.withinLastMinute();
        }
        emissionDone.await();

        assertEquals(THREADS_COUNT * EVENTS_TO_EMIT,
                systemUnderTest.withinLastHour());
    }

    private Runnable createRunnableForTest(CountDownLatch initLatch,
                                           CountDownLatch finishLatch) {
        return () -> {
            try {
                initLatch.await();
            } catch (InterruptedException e) {
                // no interruption expected in scope of this test
            }

            for (int emission = 0; emission < EVENTS_TO_EMIT; emission++) {
                systemUnderTest.track(LocalDateTime.now());
            }

            finishLatch.countDown();
        };
    }

}