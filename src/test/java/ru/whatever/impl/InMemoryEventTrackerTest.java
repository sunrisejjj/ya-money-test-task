package ru.whatever.impl;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import ru.whatever.api.exceptions.StorageLimitException;
import ru.whatever.api.model.Event;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class InMemoryEventTrackerTest {

    @InjectMocks
    private InMemoryEventTracker systemUnderTest
            = new InMemoryEventTracker();

    @Test
    public void testTrackEventThrowsExceptionIfCalledWithWrongArguments() {
        assertThrows(IllegalArgumentException.class,
                () -> systemUnderTest.track(null));
    }

    @Test
    @SuppressWarnings("unchecked")
    void testTrackEventThrowsExceptionWhenStorageIsFull() {
        // simulate full storage
        List<Event> mockedStorage = mock(List.class);
        when(mockedStorage.size()).thenReturn(Integer.MAX_VALUE);
        systemUnderTest = new InMemoryEventTracker(mockedStorage);

        assertThrows(StorageLimitException.class,
                () -> systemUnderTest.track(LocalDateTime.now()));
    }

    @Test
    void testWithinLastMinuteReturnsZeroWithoutEvents() {
        assertEquals(0L, systemUnderTest.withinLastMinute());
    }

    @Test
    void testWithinLastMinute() {
        systemUnderTest.track(LocalDateTime.now());
        systemUnderTest.track(LocalDateTime.now().minusSeconds(5));
        systemUnderTest.track(LocalDateTime.now().minusSeconds(30));
        // non-countable
        systemUnderTest.track(LocalDateTime.now()
                .minusMinutes(1).minusSeconds(1));
        systemUnderTest.track(LocalDateTime.now().minusMinutes(5));

        assertEquals(3L, systemUnderTest.withinLastMinute());
    }

    @Test
    void testWithinLastHourReturnsZeroWithoutEvents() {
        assertEquals(0L, systemUnderTest.withinLastHour());
    }

    @Test
    void testWithinLastHour() {
        systemUnderTest.track(LocalDateTime.now().minusSeconds(5));
        systemUnderTest.track(LocalDateTime.now().minusMinutes(15));
        systemUnderTest.track(LocalDateTime.now().minusMinutes(30));
        // non-countable
        systemUnderTest.track(LocalDateTime.now()
                .minusHours(1).minusMinutes(1));
        systemUnderTest.track(LocalDateTime.now().minusHours(5));

        assertEquals(3L, systemUnderTest.withinLastHour());
    }

    @Test
    void testWithinLastDayReturnsZeroWithoutEvents() {
        assertEquals(0L, systemUnderTest.withinLastDay());
    }

    @Test
    void testWithinLastDay() {
        systemUnderTest.track(LocalDateTime.now().minusSeconds(5));
        systemUnderTest.track(LocalDateTime.now().minusMinutes(30));
        systemUnderTest.track(LocalDateTime.now().minusHours(5));
        // non-countable
        systemUnderTest.track(LocalDateTime.now()
                .minusDays(1).minusMinutes(1));
        systemUnderTest.track(LocalDateTime.now().minusDays(3));

        assertEquals(3L, systemUnderTest.withinLastDay());
    }

    @Test
    void testTrackEventCleanupPerformed() {
        // initially five events must be successfully tracked
        systemUnderTest.track(LocalDateTime.now());
        systemUnderTest.track(LocalDateTime.now());
        systemUnderTest.track(LocalDateTime.now().minusSeconds(20));
        systemUnderTest.track(LocalDateTime.now().minusSeconds(30));
        systemUnderTest.track(LocalDateTime.now().minusSeconds(40));

        assertEquals(5L, systemUnderTest.withinLastMinute());

        // reconfigure event tracker to consider only two very latest events
        systemUnderTest.setEventTimeToLive(10);
        systemUnderTest.setCleanupCheckThreshold(1);
        systemUnderTest.setInitCleanupThreshold(1);

        // after reconfiguration and additional track of very latest event
        // only three very latest events must be present in storage
        systemUnderTest.track(LocalDateTime.now());
        assertEquals(3L, systemUnderTest.withinLastMinute());
    }

    @Test
    void testTrackEventCompound() {
        systemUnderTest.track(LocalDateTime.now());
        systemUnderTest.track(LocalDateTime.now());

        systemUnderTest.track(LocalDateTime.now().minusMinutes(5));
        systemUnderTest.track(LocalDateTime.now().minusMinutes(15));

        systemUnderTest.track(LocalDateTime.now().minusHours(5));
        systemUnderTest.track(LocalDateTime.now().minusHours(15));

        systemUnderTest.track(LocalDateTime.now().minusDays(5));
        systemUnderTest.track(LocalDateTime.now().minusDays(15));

        systemUnderTest.track(LocalDateTime.now());
        systemUnderTest.track(LocalDateTime.now());

        assertEquals(4L, systemUnderTest.withinLastMinute());
        assertEquals(6L, systemUnderTest.withinLastHour());
        assertEquals(8L, systemUnderTest.withinLastDay());
    }

    @Test
    void testTrackEventNonCountable() {
        systemUnderTest.track(LocalDateTime.now().minusDays(1).minusMinutes(1));
        systemUnderTest.track(LocalDateTime.now().minusDays(2));
        systemUnderTest.track(LocalDateTime.now().minusDays(3));
        systemUnderTest.track(LocalDateTime.now().minusDays(4));
        systemUnderTest.track(LocalDateTime.now().minusDays(5));

        assertEquals(0L, systemUnderTest.withinLastMinute());
        assertEquals(0L, systemUnderTest.withinLastHour());
        assertEquals(0L, systemUnderTest.withinLastDay());
    }

}